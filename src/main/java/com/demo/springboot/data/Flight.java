package com.demo.springboot.data;

import java.util.ArrayList;

public class Flight {
    private int id;
    private Airline airline;
    private String departure;
    private String arrival;
    private int capacity;
    private Airport from;
    private Airport to;
    private int price;

    public int getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public Airline getAirline() {
        return airline;
    }

    public String getDeparture() {
        return departure;
    }

    public String getArrival() {
        return arrival;
    }

    public int getCapacity() {
        return capacity;
    }

    public Airport getFrom() {
        return from;
    }

    public Airport getTo() {
        return to;
    }
}
