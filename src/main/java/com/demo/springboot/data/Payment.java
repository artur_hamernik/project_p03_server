package com.demo.springboot.data;

public class Payment {
    private String payment_type;
    private int pay_value;
    private String currency;
    private String date;
    private String status;

    public String getStatus() {
        return status;
    }

    public int getPay_value() {
        return pay_value;
    }

    public String getDate() {
        return date;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPayment_type() {
        return payment_type;
    }

}
