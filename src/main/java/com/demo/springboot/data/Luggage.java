package com.demo.springboot.data;

public class Luggage {

    private int id_luggage;
    private int weight;
    private int price;

    public int getId_luggage() {
        return id_luggage;
    }

    public int getPrice() {
        return price;
    }

    public int getWeight() {
        return weight;
    }
}
