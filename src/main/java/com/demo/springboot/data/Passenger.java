package com.demo.springboot.data;

public class Passenger {
    private int id_customer;
    private String name;
    private String surname;
    private String birth_date;
    private String sex;
    private Document document;

    public void setId_customer(int id_customer) {
        this.id_customer = id_customer;
    }

    private String login;
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public int getId_customer() {
        return id_customer;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getBirth_date() {
        return birth_date;
    }

    public String getSex() {
        return sex;
    }

    public Document getDocument() {
        return document;
    }



    public void setDocument(Document document) {
        this.document = document;
    }


}
