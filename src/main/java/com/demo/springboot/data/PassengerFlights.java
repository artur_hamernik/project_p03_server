package com.demo.springboot.data;

public class PassengerFlights {
    private Flight flight;
    private int id_passenger;
    private String arrivalDate;
    private String departureDate;
    private Luggage luggage;
    private Payment payment;

    public Flight getFlight() {
        return flight;
    }

    public Luggage getLuggage() {
        return luggage;
    }

    public Payment getPayment() {
        return payment;
    }

    public int getId_passenger() {
        return id_passenger;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getDepartureDate() {
        return departureDate;
    }
}
