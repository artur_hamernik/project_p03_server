package com.demo.springboot.data;

import com.google.cloud.firestore.annotation.PropertyName;

public class Airport {
    private String name;
    private String location;
    private String timezone;

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getTimezone() {
        return timezone;
    }
}
