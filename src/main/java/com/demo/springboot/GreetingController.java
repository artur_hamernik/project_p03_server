package com.demo.springboot;

import com.demo.springboot.data.*;
import com.demo.springboot.service.FirebaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api")
public class GreetingController {
    @Autowired
    FirebaseService firebaseService;

    @GetMapping("/customerDetails")
    public Passenger getCustomer(@RequestParam String name, String surname) throws ExecutionException, InterruptedException {
        return firebaseService.getPassenger(name,surname);
    }
    @PostMapping("/postPassenger")
    public String postCustomer(@RequestBody Passenger passenger) throws ExecutionException, InterruptedException {
        return firebaseService.postPassenger(passenger);
    }
    @GetMapping("/flight")
    public Flight getFlights(@RequestParam String name) throws ExecutionException, InterruptedException {
        return firebaseService.getFlight(name);
    }
    @PostMapping("/createFlight")
    public String postFlight(@RequestBody Flight flight) throws ExecutionException, InterruptedException {
        return firebaseService.saveFlight(flight);
    }
    @GetMapping("/passfly")
    public ArrayList<PassengerFlights> getPassFly(@RequestParam int id_passenger) throws ExecutionException, InterruptedException {
        return firebaseService.getPassengerFlights(id_passenger);
    }
    @PostMapping("/postpassfly")
    public String postPassFly(@RequestBody PassengerFlights passengerFlights) throws ExecutionException, InterruptedException {
        return firebaseService.savePassengerFlights(passengerFlights);
    }
    @GetMapping("/flights")
    public ArrayList<Flight> getFlights() throws ExecutionException, InterruptedException {
        return firebaseService.getFlights();
    }
    @GetMapping("/whereflights")
    public ArrayList<Flight> getSpecificFlights(@RequestParam String from, String to) throws ExecutionException, InterruptedException {
        return firebaseService.getSpecificFlights(from,to);
    }
    @GetMapping("/user")
    public Passenger getUser(@RequestParam String login, String password) throws ExecutionException, InterruptedException {
        return firebaseService.getPassengerByLogin(login, password);
    }
    @GetMapping("/passengers")
    public ArrayList<Passenger> getPassengers() throws ExecutionException, InterruptedException {
        return firebaseService.getPassengers();
    }
    @GetMapping("/luggage")
    public Luggage getLuggage(@RequestParam int weight) throws ExecutionException, InterruptedException {
        return firebaseService.getLuggage(weight);
    }
    @PostMapping("/putpassfly")
    public String putPassFly(@RequestBody PassengerFlights passengerFlights) throws ExecutionException, InterruptedException {
        return firebaseService.putPassengerFlights(passengerFlights);
    }
}
