package com.demo.springboot.data;

public class Document {
    private String document_serial;
    private String document_type;
    private String expiration_date;
    private String citizenship;


    public String getDocument_serial() {
        return document_serial;
    }

    public String getDocument_type() {
        return document_type;
    }

    public String getExpiration_date() {
        return expiration_date;
    }

    public String getCitizenship() {
        return citizenship;
    }
}
