package com.demo.springboot.service;

import com.demo.springboot.data.*;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firestore.admin.v1.Field;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class FirebaseService {
    private final AtomicLong counterPassengersIds = new AtomicLong();

    public String postPassenger(Passenger passenger) throws ExecutionException, InterruptedException {
        int counterId = (int) counterPassengersIds.incrementAndGet();
        passenger.setId_customer(counterId);
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionApiFuture = dbFirestore.collection("Passenger").document(String.valueOf(counterId)).set(passenger);
        return collectionApiFuture.get().getUpdateTime().toString();
    }

    public Passenger getPassenger(String name, String surname) throws ExecutionException, InterruptedException {
        String id = name+surname;
        System.out.println(id);
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection("Passenger").document(id);
        ApiFuture<DocumentSnapshot> future = documentReference.get();
        DocumentSnapshot document = future.get();
        Passenger passenger;

        if(document.exists()){
            passenger = document.toObject(Passenger.class);
            return passenger;
        }
        else
            return null;
    }

    public Flight getFlight(String name) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection("Flight").document(name);
        ApiFuture<DocumentSnapshot> future = documentReference.get();

        DocumentSnapshot document = future.get();

        Flight flight;

        if(document.exists()){
            flight = document.toObject(Flight.class);
            return flight;
        }
        else
            return null;
    };

    public String saveFlight(Flight flight) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionApiFuture = dbFirestore.collection("Flight").document(String.valueOf(flight.getId())).set(flight);
        return collectionApiFuture.get().getUpdateTime().toString();
    }

    public String savePassengerFlights(PassengerFlights passengerFlights) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionApiFuture = dbFirestore.collection("PassengerFlights")
                .document(passengerFlights.getId_passenger()+"_"+passengerFlights.getFlight().getId())
                .set(passengerFlights);
        return collectionApiFuture.get().getUpdateTime().toString();
    }

    public ArrayList<PassengerFlights> getPassengerFlights(int id_passenger) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        //asynchronously retrieve all documents
        ApiFuture<QuerySnapshot> future = dbFirestore.collection("PassengerFlights").whereEqualTo(FieldPath.of("id_passenger"), id_passenger).get();
        // future.get() blocks on response
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        ArrayList<PassengerFlights> passengerFlights = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            passengerFlights.add(document.toObject(PassengerFlights.class));
        }

        return passengerFlights;
    }
    public ArrayList<Flight> getFlights() throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        //asynchronously retrieve all documents
        ApiFuture<QuerySnapshot> future = dbFirestore.collection("Flight").get();
        // future.get() blocks on response
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        ArrayList<Flight> flights = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            flights.add(document.toObject(Flight.class));
        }

        return flights;
    }
    public  ArrayList<Flight> getSpecificFlights(String from, String to) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> future =
                dbFirestore.collection("Flight").whereEqualTo(FieldPath.of("from","name"), from).whereEqualTo(FieldPath.of("to","name"), to).get();
        ArrayList<Flight> flights = new ArrayList<>();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for (DocumentSnapshot document : documents) {
            flights.add(document.toObject(Flight.class));
        }
        return flights;
    }
    public Passenger getPassengerByLogin(String login , String password) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> future =
                dbFirestore.collection("Passenger").whereEqualTo("login", login).whereEqualTo("password", password).get();

        ArrayList<Passenger> user = new ArrayList<>();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for (DocumentSnapshot document : documents) {
            user.add(document.toObject(Passenger.class));
        }
        return user.get(0);
    }
    public ArrayList<Passenger> getPassengers() throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        //asynchronously retrieve all documents
        ApiFuture<QuerySnapshot> future = dbFirestore.collection("Passenger").get();
        // future.get() blocks on response
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        ArrayList<Passenger> passengers = new ArrayList<>();
        for (QueryDocumentSnapshot document : documents) {
            passengers.add(document.toObject(Passenger.class));
        }

        return passengers;
    }
    public Luggage getLuggage(int weight) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<QuerySnapshot> future =
                dbFirestore.collection("Luggage").whereEqualTo("weight", weight).get();

        ArrayList<Luggage> luggages = new ArrayList<>();
        List<QueryDocumentSnapshot> documents = future.get().getDocuments();
        for (DocumentSnapshot document : documents) {
            luggages.add(document.toObject(Luggage.class));
        }
        return luggages.get(0);
    }

    public String putPassengerFlights(PassengerFlights passengerFlights) throws ExecutionException, InterruptedException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionApiFuture = dbFirestore.collection("PassengerFlights")
                .document(passengerFlights.getId_passenger()+"_"+passengerFlights.getFlight().getId())
                .set(passengerFlights);
        return collectionApiFuture.get().getUpdateTime().toString();
    }
}